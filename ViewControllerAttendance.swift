//
//  ViewControllerAttendance.swift
//  MOBI KIET Attendance
//
//  Created by Kunwar Anirudh Singh on 18/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//

import UIKit

class ViewControllerAttendance: UIViewController {

    
    @IBOutlet weak var DateView: UIView!
    @IBOutlet weak var Submit: UIButton!
    
    @IBOutlet weak var StartDate: UITextField!
    @IBOutlet weak var EndDate: UITextField!
    
    
    let datePickerViewFrom = UIDatePicker()
    let datePickerViewTo = UIDatePicker()
    
    
    func CreateDatePickerFrom() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss" //Your date format
        
        datePickerViewFrom.datePickerMode = .date
        datePickerViewFrom.maximumDate = Date()
        datePickerViewFrom.minimumDate = dateFormatter.date(from: "2017-07-01 00:00:00")
        
        
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(DonePressed))
        toolbar.setItems([doneButton], animated: false)
        
        StartDate.inputAccessoryView = toolbar
        StartDate.inputView = datePickerViewFrom
        
    }
    
    func CreateDatePickerTo() {
        datePickerViewTo.datePickerMode = .date
        datePickerViewTo.maximumDate = Date()
        datePickerViewTo.minimumDate = datePickerViewFrom.date
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(DonePressed))
        toolbar.setItems([doneButton], animated: false)
        
        EndDate.inputAccessoryView = toolbar
        EndDate.inputView = datePickerViewTo
        
    }
    
    func DonePressed() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        StartDate.text = dateFormatter.string(from: datePickerViewFrom.date)
        EndDate.text = dateFormatter.string(from: datePickerViewTo.date)
        
        CreateDatePickerTo()
        CreateDatePickerFrom()
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //set Today Date in To
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        EndDate.text = dateFormatter.string(from: Date())
        
        //For Button Shadow
        Submit.layer.shadowColor = UIColor.black.cgColor
        Submit.layer.shadowOffset = CGSize(width:0,height:0)
        Submit.layer.shadowOpacity = 0.4
        Submit.layer.shadowRadius = 5
        
        //For DateView Shadow
        DateView.layer.shadowColor = UIColor.black.cgColor
        DateView.layer.shadowOffset = CGSize(width: 0,height: 0)
        DateView.layer.shadowOpacity = 0.4
        DateView.layer.shadowRadius = 5
        DateView.layer.cornerRadius = 3
        
        //For Background
        self.view.backgroundColor = UIColor.init(red: 45.0/255.0, green: 93.0/255.0, blue: 131.0/255.0, alpha: 1)
        
        self.navigationController?.navigationBar.isHidden = true
        
        
        
        CreateDatePickerFrom()
        CreateDatePickerTo()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowAttendance") {
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
