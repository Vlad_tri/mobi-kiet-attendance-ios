//
//  AttendanceDetailViewController.swift
//  MOBI KIET Attendance
//
//  Created by Kunwar Anirudh Singh on 19/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//

import UIKit
import UICircularProgressRing
import SCLAlertView

class AttendanceDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var Subject: UILabel!
    var Sub : String!
    @IBOutlet weak var Total: UILabel!
    var Tdata : Int!
    @IBOutlet weak var Present: UILabel!
    var Pdata : Int!
    @IBOutlet weak var Absent: UILabel!
    var Adata : Int!
    @IBOutlet weak var progressRing: UICircularProgressRingView!
    @IBOutlet weak var LectureDates: UILabel!
    @IBOutlet weak var PA: UILabel!
    @IBOutlet weak var AttendanceTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //For Passed Data
        Subject.text = Sub
        Total.text = String(Tdata)
        Present.text = String(Pdata)
        Absent.text = String(Adata)
        
        //For Ring
        progressRing.maxValue = 100
        progressRing.innerRingColor = UIColor.blue
        progressRing.innerRingSpacing = 20
        progressRing.innerRingWidth = 10
        progressRing.fontColor = UIColor.white
        
        //For Custom Table View
        AttendanceTable.delegate = self
        AttendanceTable.dataSource = self
        
        // For Main Background
        self.view.backgroundColor = UIColor.init(red: 45.0/255.0, green: 93.0/255.0, blue: 131.0/255.0, alpha: 1)
        
        // For Navigation Bar
        self.navigationItem.title = "Attendance"
        let rightBarButtonItem:UIBarButtonItem = UIBarButtonItem(title: "Info", style: UIBarButtonItemStyle.plain, target:self, action: #selector(popinfo))
        self.navigationItem.setRightBarButtonItems([rightBarButtonItem], animated: true)

    }
    
    func popinfo() {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert = SCLAlertView(appearance: appearance)
        
        // Creat the subview
        let subview = UIView(frame: CGRect(x: 0,y : 0,width: 350, height: 100))
        
        // Add Label
        let ColorLabel1 = UILabel(frame: CGRect(x: 20, y: 10, width: 15, height: 15))
        ColorLabel1.layer.backgroundColor = UIColor.purple.cgColor
        let TextLabel1 = UILabel(frame: CGRect(x: 50, y: 6, width: 250, height: 25))
        TextLabel1.font = TextLabel1.font.withSize(12)
        TextLabel1.text = "Extra Attendance"
        
        let ColorLabel2 = UILabel(frame: CGRect(x: 20, y: 30, width: 15, height: 15))
        ColorLabel2.layer.backgroundColor = UIColor.purple.cgColor
        let TextLabel2 = UILabel(frame: CGRect(x: 50, y: 26, width: 250, height: 25))
        TextLabel2.font = TextLabel1.font.withSize(12)
        TextLabel2.text = "Special Attendance"
        
        let ColorLabel3 = UILabel(frame: CGRect(x: 20, y: 50, width: 15, height: 15))
        ColorLabel3.layer.backgroundColor = UIColor.purple.cgColor
        let TextLabel3 = UILabel(frame: CGRect(x: 50, y: 46, width: 250, height: 25))
        TextLabel3.font = TextLabel1.font.withSize(12)
        TextLabel3.text = "Update Attendance"
        
        let ColorLabel4 = UILabel(frame: CGRect(x: 20, y: 70, width: 15, height: 15))
        ColorLabel4.layer.backgroundColor = UIColor.purple.cgColor
        let TextLabel4 = UILabel(frame: CGRect(x: 50, y: 66, width: 250, height: 25))
        TextLabel4.font = TextLabel1.font.withSize(12)
        TextLabel4.text = "Group Attendance"
        
        subview.addSubview(ColorLabel1)
        subview.addSubview(TextLabel1)
        subview.addSubview(ColorLabel2)
        subview.addSubview(TextLabel2)
        subview.addSubview(ColorLabel3)
        subview.addSubview(TextLabel3)
        subview.addSubview(ColorLabel4)
        subview.addSubview(TextLabel4)

        alert.customSubview = subview
        alert.addButton("Ok") {}
        alert.showInfo("Attendance Types", subTitle: "", duration: 10)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Tdata
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPA", for: indexPath)
        return cell
    }
    
    func CircularRing() {
        let T = Tdata
        let P = Pdata
        let per:CGFloat = ceil(CGFloat(P!) / CGFloat(T!) * 100)
        progressRing.setProgress(value: per, animationDuration: 3)
        
        if (per < 60) {
            progressRing.innerRingColor = UIColor.red
        } else if (per < 75) {
            progressRing.innerRingColor = UIColor.orange
        } else {
            progressRing.innerRingColor = UIColor.init(red: 0.0, green: 130.0/255.0, blue: 0.0, alpha: 1)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        CircularRing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
