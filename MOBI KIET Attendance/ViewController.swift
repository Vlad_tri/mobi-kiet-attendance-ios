//
//  ViewController.swift
//  MOBI KIET Attendance
//
//  Created by Kunwar Anirudh Singh on 17/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//
import UIKit
import UICircularProgressRing

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var attendanceTable: UITableView!
    @IBOutlet weak var Total: UILabel!
    @IBOutlet weak var PresentLabel: UILabel!
    @IBOutlet weak var AbsentLabel: UILabel!
    @IBOutlet weak var progressRing: UICircularProgressRingView!
    
    // Static Data
    var names = ["DESIGN AND ANALYSIS OF ALGORITHM","DATABASE MANAGEMENT SYSTEM","PRINCIPLE OF PROGRAMMING LANGUAGE","WEB TECHNOLOGY", "MANAGEMENT INFORMATION SYSTEM","ENGINEERING ECONOMICS","SOFT SKILLS"]
    var attendance = [74,58,85,73,80,67,500]
    var present = [11,11,11,13,6,4,8]
    var absent = [4,8,6,5,6,2,2]
    var lectures = [15,19,17,18,12,6,4]
    
    
    func startCount(count:Float,progressView:UIProgressView) {
        let i = count
        let max = Float(100)
            var ratio = Float(i) / Float(max)
            progressView.progress = Float(ratio)
            ratio *= 100
            if (ratio < 60.0) {
                progressView.progressTintColor = UIColor.red
            }
            else if (ratio < 75) {
                progressView.progressTintColor = UIColor.orange
            }
            else {
                progressView.progressTintColor = UIColor.init(red: 0.0, green: 130.0/255.0, blue: 0.0, alpha: 1)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(5)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AttendanceTableViewCell
        cell.Subject.text = names[indexPath.section]
        cell.A.text = String("A:\(absent[indexPath.section])")
        cell.P.text = String("P:\(present[indexPath.section])")
        cell.L.text = String("L:\(lectures[indexPath.section])")
        cell.Percent.text = String("\(attendance[indexPath.section])%")
        startCount(count: Float(attendance[indexPath.section]), progressView: cell.CellProgress)
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowDetail") {
            let indexPath = self.attendanceTable.indexPathForSelectedRow
            if  ((indexPath) != nil) {
                let attendanceDetailPage = segue.destination as! AttendanceDetailViewController
                attendanceDetailPage.Sub = names[(indexPath?.section)!]
                attendanceDetailPage.Tdata = lectures[(indexPath?.section)!]
                attendanceDetailPage.Pdata = present[(indexPath?.section)!]
                attendanceDetailPage.Adata = absent[(indexPath?.section)!]
            }
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //For Ring
        progressRing.maxValue = 100
        progressRing.innerRingColor = UIColor.blue
        progressRing.innerRingSpacing = 20
        progressRing.innerRingWidth = 10
        progressRing.fontColor = UIColor.white
        
        //For Custom Table View
        attendanceTable.delegate = self
        attendanceTable.dataSource = self
        
        // For Main Background
        self.view.backgroundColor = UIColor.init(red: 45.0/255.00, green: 93.0/255.0, blue: 131.0/255.0, alpha: 1)
        
        // For Navigation Bar
        self.navigationItem.title = "Attendance"
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        Total.text = String(lectures.reduce(0, +))
        AbsentLabel.text = String(absent.reduce(0,+))
        PresentLabel.text = String(present.reduce(0,+))
        CircularRing()
    }
    
    func CircularRing() {
        let T = Int(lectures.reduce(0, +))
        let P = Int(present.reduce(0, +))
        let per:CGFloat = CGFloat(P) / CGFloat(T) * 100
        progressRing.setProgress(value: per, animationDuration: 3)
        
        if (per < 60) {
            progressRing.innerRingColor = UIColor.red
        } else if (per < 75) {
            progressRing.innerRingColor = UIColor.orange
        } else {
            progressRing.innerRingColor = UIColor.init(red: 0.0, green: 130.0/255.0, blue: 0.0, alpha: 1)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

