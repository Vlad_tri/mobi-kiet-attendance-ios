//
//  AttendanceTableViewCell.swift
//  MOBI KIET Attendance
//
//  Created by Kunwar Anirudh Singh on 18/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//

import UIKit

class AttendanceTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var Subject: UILabel!
    @IBOutlet weak var P: UILabel!
    @IBOutlet weak var A: UILabel!
    @IBOutlet weak var L: UILabel!
    @IBOutlet weak var CellProgress: UIProgressView!
    @IBOutlet weak var Percent: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backView.layer.masksToBounds = true
        
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
