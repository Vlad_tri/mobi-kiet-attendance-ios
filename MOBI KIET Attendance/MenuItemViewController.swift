//
//  MenuItemViewController.swift
//  MOBI KIET Attendance
//
//  Created by Kunwar Anirudh Singh on 20/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//

import UIKit

class MenuItemViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //Static Data
    var ItemNamesSection1 = ["Dashboard","Attendance","Marks","Student Grievance","Time Table","Academic Calendar"]
    var ItemNamesSection2 = ["About","Logout"]
    
    var revealController: RevealViewController?
    
    
    
    @IBOutlet weak var MenuItemTable: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        revealController = self.revealViewController() as? RevealViewController

        // Do any additional setup after loading the view.
        MenuItemTable.dataSource = self
        MenuItemTable.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    
    let darkView = UIView()
    
    override func viewWillAppear(_ animated: Bool) {
        
        darkView.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        darkView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        darkView.frame = self.revealViewController().frontViewController.view.bounds
        self.revealViewController().frontViewController.view.addSubview(darkView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        darkView.removeFromSuperview()
    }
    
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return ItemNamesSection1.count
        } else if (section == 1) {
            return ItemNamesSection2.count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = MenuItemTable.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! MenuItemTableViewCell
        if (indexPath.section == 0) {
            cell.ItemName.text = ItemNamesSection1[indexPath.row]
        } else if (indexPath.section == 1) {
            cell.ItemName.text = ItemNamesSection2[indexPath.row]
        }
        
        
        let navController = UINavigationController()
        let revealController = SWRevealViewController()
        revealController.rightViewController = navController
        revealController.revealToggle(animated: true)
        revealController.rightViewController.view.addGestureRecognizer(revealController.panGestureRecognizer())
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) { return CGFloat(0) }
        return CGFloat(1)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0 && indexPath.section == 0) {
            print("1")
            self.performSegue(withIdentifier: "AttendanceVC", sender: AnyObject.self)
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
