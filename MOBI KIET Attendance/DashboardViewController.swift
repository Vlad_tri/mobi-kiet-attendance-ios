//
//  DashboardViewController.swift
//  MOBI KIET Attendance
//
//  Created by Kunwar Anirudh Singh on 20/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

   
    @IBOutlet weak var WelcomeView: UIView!
    @IBOutlet weak var RingView: UIView!
    @IBOutlet weak var InfoView: UIView!

    
    
    
    
    @IBAction func SideBar(_ sender: UIButton) {
        let revealController = self.revealViewController() as! RevealViewController
        revealController.reveal(sender)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //For WelcomeView
        WelcomeView.layer.shadowColor = UIColor.black.cgColor
        WelcomeView.layer.shadowOffset = CGSize(width: 0,height: 0)
        WelcomeView.layer.shadowOpacity = 0.4
        WelcomeView.layer.shadowRadius = 5
        WelcomeView.layer.cornerRadius = 3
        
        //For RingView
        RingView.layer.shadowColor = UIColor.black.cgColor
        RingView.layer.shadowOffset = CGSize(width: 0,height: 0)
        RingView.layer.shadowOpacity = 0.4
        RingView.layer.shadowRadius = 5
        RingView.layer.cornerRadius = 3
        
        //For InfoView
        InfoView.layer.shadowColor = UIColor.black.cgColor
        InfoView.layer.shadowOffset = CGSize(width: 0,height: 0)
        InfoView.layer.shadowOpacity = 0.4
        InfoView.layer.shadowRadius = 5
        InfoView.layer.cornerRadius = 3
        
        self.navigationController?.isNavigationBarHidden = true

        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
